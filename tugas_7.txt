1. Membuat Database
	
	CREATE DATABASE myshop;

2. Membuat tabel di dalam database

	tabel users
	CREATE TABLE users ( id int(10) PRIMARY KEY AUTO_INCREMENT, nama varchar(255) NOT NULL, email varchar(255) NOT NULL, password varchar(255) NOT NULL );

	tabel categories
	CREATE TABLE categories ( id int(10) PRIMARY KEY AUTO_INCREMENT, nama varchar(255) NOT NULL);

	tabel items
	CREATE TABLE items ( id int(10) PRIMARY KEY AUTO_INCREMENT, nama varchar(255) NOT NULL, description varchar(255) NOT NULL, price int NOT NULL, stock int NOT NULL, category_id int NOT NULL, FOREIGN KEY(category_id) REFERENCES categories(id) );

3. Insert Data ke dalam tabel

	insert data users
	INSERT INTO users(nama, email, password) VALUES ("John Doe", "john@doe.com", "john123"), ("Jane Doe", "jane@doe.com", "jenita123");

	insert data categories
	INSERT INTO categories(nama) VALUES ("gadget"), ("cloth"), ("men"), ("women"), ("branded");

	insert data items
	INSERT INTO items(nama,description,price,stock,category_id) VALUES ("Sumsang b50", "hape keren dari merek sumsang", 4000000, 100, 1), ("Uniklooh", "naju keren dari brand ternama", 500000, 50, 2), ("IMHO Watch", "jam tangan anak jujur yang banget", 2000000, 10, 1);


4. Mengambil data dari database

	a. mengambil data users
	SELECT id, nama, email FROM users;

	b mengambil data items
	SELECT * FROM `items` WHERE price > 1000000;
	SELECT * FROM `items` WHERE nama LIKE "%uniklo%";

	c. menampilkan data items join dgn kategori
	SELECT items.nama , items.description, items.price, items.stock, items.category_id, categories.nama FROM items INNER JOIN categories ON items.category_id = categories.id;

5. Mengubah data dari database

	SELECT items.nama , items.description, items.price, items.stock, items.category_id, categories.nama FROM items INNER JOIN categories ON items.category_id = categories.id;
